package git

import (
	"gopkg.in/src-d/go-git.v4"
)

// GetBranchName returns HEAD's branch name for git repo at path
func GetBranchName(path string) (string, error) {
	repo, err := git.PlainOpen(path)
	if err != nil {
		return "", err
	}

	head, err := repo.Head()
	if err != nil {
		return "", err
	}
	return head.Name().Short(), nil
}
