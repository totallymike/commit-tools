// Copyright © 2018 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"

	"bytes"
	"github.com/spf13/cobra"
	git "github.com/totallymike/commit-tools/git"
	"io"
	"os"
	"regexp"
	"strings"
)

// MessageSource represents the type of commit being handled.
type MessageSource = string

// These are the possible types of commits for which messages are generated.
const (
	MessageMessage  MessageSource = "message"
	TemplateMessage MessageSource = "template"
	MergeMessage    MessageSource = "merge"
	SquashMessage   MessageSource = "squash"
	NoMessage       MessageSource = "none"
)

// PrepareCommitMessageOptions is a dumping ground for setup stuff.
type PrepareCommitMessageOptions struct {
	prefix        string
	repoPath      string
	messageFile   string
	messageSource MessageSource
	commitSha     string
}

// Complete fills out the options from the args, or produces an error.
func (o *PrepareCommitMessageOptions) Complete(cmd *cobra.Command, args []string) error {
	o.prefix = args[0]

	wd, err := os.Getwd()
	if err != nil {
		return err
	}
	o.repoPath = wd

	o.messageFile = args[1]

	switch len(args) {
	case 0, 1, 2:
		o.messageSource = NoMessage
		o.commitSha = ""
	case 3:
		if strings.Compare(args[2], "") == 0 {
			o.messageSource = NoMessage
		} else {
			o.messageSource = args[2]
		}
		o.commitSha = ""
	case 4:
		o.messageSource = args[2]
		o.commitSha = args[3]
	}

	return nil
}

// RunCommand executes the actual command.
func (o *PrepareCommitMessageOptions) RunCommand(cmd *cobra.Command) error {
	branchName, err := git.GetBranchName(o.repoPath)

	if err != nil {
		return err
	}

	if !strings.HasPrefix(branchName, o.prefix) {
		return nil
	}

	pattern, err := createPattern(o.prefix)
	if err != nil {
		return err
	}

	matches := pattern.FindStringSubmatch(branchName)
	if len(matches) < 1 {
		fmt.Fprintln(cmd.OutOrStderr(), "wat")
		return nil
	}

	ticketNumber := matches[1]

	switch o.messageSource {
	case NoMessage:
		file, err := os.OpenFile(o.messageFile, os.O_RDWR, 0755)
		if err != nil {
			return err
		}
		stat, err := file.Stat()
		if err != nil {
			return err
		}
		buffer := make([]byte, stat.Size())

		if _, err := file.Read(buffer); err != nil {
			return err
		}

		if _, err := file.Seek(0, io.SeekStart); err != nil {
			return err
		}

		if _, err := file.WriteString(ticketNumber); err != nil {
			return err
		}
		if _, err := file.Write(buffer); err != nil {
			return err
		}

		if err := file.Close(); err != nil {
			return err
		}
	}

	fmt.Fprintf(cmd.OutOrStdout(), "Ticket number: %v\n", ticketNumber)

	return nil
}

func createPattern(prefix string) (*regexp.Regexp, error) {
	var b bytes.Buffer
	b.WriteString("(")
	b.WriteString(prefix)
	b.WriteString(`-\d+)/`)

	return regexp.Compile(b.String())
}

func init() {
	o := &PrepareCommitMessageOptions{}

	// prepareCommitMsgCmd represents the prepareCommitMsg command
	var prepareCommitMsgCmd = &cobra.Command{
		Use:   "prepare-commit-msg [suffix] [temporary_file] [commit_type] [sha]",
		Short: "Prepare a commit message",
		Long: `Prepares a commit message for the current repository, based on the branch name.

Pass in a suffix, and optionally the path to a repository. The suffix will be used to infer
the ticket number.

The rest of the arguments are for git to deal with, not you :)

It expects a ticket number, followed by a slash, followed by the name of the branch.
Like so:

    commit-tools prepare-commit-msg TTTT <file> <commit_type> <sha>

In a branch called TTTT-123/my-branch, it then prepends TTTT-123 to each commit message.`,
		Args: cobra.RangeArgs(1, 4),
		Run: func(cmd *cobra.Command, args []string) {
			if err := o.Complete(cmd, args); err != nil {
				fmt.Fprintln(cmd.OutOrStderr(), err)
				os.Exit(1)
			}

			if err := o.RunCommand(cmd); err != nil {
				fmt.Fprintln(cmd.OutOrStderr(), err)
				os.Exit(1)
			}
		},
		DisableFlagsInUseLine: true,
	}

	rootCmd.AddCommand(prepareCommitMsgCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// prepareCommitMsgCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// prepareCommitMsgCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
